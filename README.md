# tuifp

![Screenshot of tuifp](assets/tuifp.png)

## About

`tuifp` is a small [ratatui](https://ratatui.rs)-based TUI client for playing
the <https://musicforprogramming.net> series of soundtrack mixes.

## Install

### Dependencies

- Development libraries for `mpv`, e.g. the `libmpv-devel` package on Debian-like distros

### From source

Clone this repo and run:

```console
cargo install --locked --path .
```

## Usage

### Controls

|Key          |Description                           |
|-------------|--------------------------------------|
|↑ / `k`      |Move up the episode list              |
|↓ / `j`      |Move down the episode list            |
|(`Shift` +) ←|Seek backward (30s / 10m with `Shift`)|
|(`Shift` +) →|Seek forward (30s / 10m with `Shift`) |
|`Enter`      |Play selected episode                 |
|`Space`      |Toggle pause                          |
|`m`          |Toggle sound mute                     |
|`q`          |Quit                                  |
