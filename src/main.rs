use std::time::Duration;

use anyhow::{Context, Result as AhResult};
use feed_rs::model::Feed;
use feed_rs::parser;
use libmpv2::events::{Event as MpvEvent, EventContext, PropertyData};
use libmpv2::{FileState, Format, Mpv};
use ratatui::backend::CrosstermBackend;
use ratatui::crossterm::event::{
    DisableMouseCapture, EnableMouseCapture, Event, KeyCode, KeyEvent, KeyModifiers,
};
use ratatui::crossterm::terminal::{
    disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen,
};
use ratatui::crossterm::{event, execute};
use ratatui::layout::{Alignment, Constraint, Flex, Layout};
use ratatui::style::{Modifier, Style};
use ratatui::text::Line;
use ratatui::widgets::{List, ListState};
use ratatui::{DefaultTerminal, Frame, Terminal, TerminalOptions, Viewport};
use scopeguard::defer;

const MFP_URL: &str = "https://musicforprogramming.net/rss.xml";

#[derive(Debug)]
struct Episode {
    number: u32,
    title: String,
    url: String,
}

struct App {
    episodes: Vec<Episode>,
    list_state: ListState,
    player_handle: Mpv,
    title: Option<String>,
    position: Option<i64>,
    duration: Option<i64>,
    paused: bool,
    muted: bool,
}

fn main() -> AhResult<()> {
    enable_raw_mode()?;
    defer! {
        let _ = disable_raw_mode();
    }

    // fetch episode feed and draw a wait prompt
    let stdout = std::io::stdout();
    let backend = CrosstermBackend::new(stdout);
    let mut inline_terminal = Terminal::with_options(
        backend,
        TerminalOptions {
            viewport: Viewport::Inline(1),
        },
    )?;

    inline_terminal.draw(|f| {
        let area = f.area();
        let text = Line::from("Loading episodes...\n");
        f.render_widget(text, area);
    })?;

    let feed = {
        defer! {
            let _ = inline_terminal.clear();
        }
        fetch_feed().context("failed to load episodes")?
    };

    // prepare episodes and start the player
    let episodes: Vec<_> = feed
        .entries
        .iter()
        .map(|e| {
            let title_raw = &e.title.as_ref().unwrap().content;
            let stripped = title_raw.strip_prefix("Episode ").unwrap();
            let (num, title) = stripped.split_once(": ").unwrap();

            let number = num.parse().unwrap();
            let title = title.to_string();

            let url = e.id.clone();

            Episode { number, title, url }
        })
        .collect();

    let list_state = ListState::default().with_selected(Some(0));
    let player_handle = Mpv::with_initializer(|init| {
        init.set_option("config", "yes").unwrap();
        init.set_property("volume", 100).unwrap();
        Ok(())
    })
    .unwrap();

    {
        let ev_ctx = EventContext::new(player_handle.ctx);
        ev_ctx
            .observe_property("duration", Format::Int64, 1)
            .unwrap();
        ev_ctx
            .observe_property("time-pos", Format::Int64, 1)
            .unwrap();
        ev_ctx.observe_property("pause", Format::Flag, 1).unwrap();
        ev_ctx.observe_property("mute", Format::Flag, 1).unwrap();
    }

    let app = App {
        episodes,
        list_state,
        player_handle,
        title: None,
        position: None,
        duration: None,
        paused: false,
        muted: false,
    };

    let stdout = std::io::stdout();
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    execute!(
        terminal.backend_mut(),
        EnterAlternateScreen,
        EnableMouseCapture
    )?;
    let mut terminal = scopeguard::guard(terminal, |mut t| {
        let _ = execute!(t.backend_mut(), DisableMouseCapture, LeaveAlternateScreen);
    });

    run(&mut terminal, app)
}

fn fetch_feed() -> AhResult<Feed> {
    let response = minreq::get(MFP_URL).send()?;
    let rss = response.as_bytes();

    parser::parse(rss).map_err(Into::into)
}

fn poll_event() -> AhResult<Option<Event>> {
    let tick_rate = Duration::from_millis(50);
    if !event::poll(tick_rate)? {
        return Ok(None);
    }

    event::read().map(Some).map_err(Into::into)
}

fn run(terminal: &mut DefaultTerminal, mut app: App) -> AhResult<()> {
    loop {
        terminal.draw(|f| render(f, &mut app))?;

        match poll_event()? {
            Some(Event::Key(KeyEvent {
                code: KeyCode::Char('q') | KeyCode::Esc,
                ..
            })) => {
                return Ok(());
            }
            Some(Event::Key(KeyEvent {
                code: KeyCode::Char('k') | KeyCode::Up,
                ..
            })) => {
                let len = app.episodes.len();
                let old_select = app.list_state.selected().unwrap();
                let new_select = (old_select + len - 1) % len;
                app.list_state.select(Some(new_select));
            }
            Some(Event::Key(KeyEvent {
                code: KeyCode::Char('j') | KeyCode::Down,
                ..
            })) => {
                let len = app.episodes.len();
                let old_select = app.list_state.selected().unwrap();
                let new_select = (old_select + 1) % len;
                app.list_state.select(Some(new_select));
            }
            Some(Event::Key(KeyEvent {
                code: KeyCode::Left,
                modifiers: KeyModifiers::SHIFT,
                ..
            })) => {
                let _ = app.player_handle.seek_backward(600.);
            }
            Some(Event::Key(KeyEvent {
                code: KeyCode::Right,
                modifiers: KeyModifiers::SHIFT,
                ..
            })) => {
                let _ = app.player_handle.seek_forward(600.);
            }
            Some(Event::Key(KeyEvent {
                code: KeyCode::Left,
                ..
            })) => {
                let _ = app.player_handle.seek_backward(30.);
            }
            Some(Event::Key(KeyEvent {
                code: KeyCode::Right,
                ..
            })) => {
                let _ = app.player_handle.seek_forward(30.);
            }
            Some(Event::Key(KeyEvent {
                code: KeyCode::Enter,
                ..
            })) => {
                let selection = app.list_state.selected().unwrap();
                let ep = &app.episodes[selection];
                let num = ep.number;
                let title = &ep.title;
                let url = &ep.url;
                app.player_handle
                    .playlist_load_files(&[(url, FileState::Replace, None)])
                    .unwrap();
                let _ = app.player_handle.unpause();
                app.title = Some(format!("{:02} {}", num, title));
            }
            Some(Event::Key(KeyEvent {
                code: KeyCode::Char(' '),
                ..
            })) => {
                let _ = app.player_handle.cycle_property("pause", true);
            }
            Some(Event::Key(KeyEvent {
                code: KeyCode::Char('m'),
                ..
            })) => {
                let _ = app.player_handle.cycle_property("mute", true);
            }
            _ => (),
        }

        let mut ev_ctx = EventContext::new(app.player_handle.ctx);
        let Some(ev) = ev_ctx.wait_event(0.05) else {
            continue;
        };

        match ev {
            Ok(MpvEvent::EndFile(_)) => {
                app.position = None;
                app.duration = None;
            }
            Ok(MpvEvent::PropertyChange {
                name: "duration",
                change: PropertyData::Int64(duration),
                ..
            }) => {
                app.duration = Some(duration);
            }
            Ok(MpvEvent::PropertyChange {
                name: "time-pos",
                change: PropertyData::Int64(position),
                ..
            }) => {
                app.position = Some(position);
            }
            Ok(MpvEvent::PropertyChange {
                name: "pause",
                change: PropertyData::Flag(paused),
                ..
            }) => {
                app.paused = paused;
            }
            Ok(MpvEvent::PropertyChange {
                name: "mute",
                change: PropertyData::Flag(muted),
                ..
            }) => {
                app.muted = muted;
            }
            _ => (),
        }
    }
}

fn format_time(seconds: i64) -> (i64, i64, i64) {
    let hours = seconds / 3600;
    let mins = (seconds % 3600) / 60;
    let secs = (seconds % 3600) % 60;

    (hours, mins, secs)
}

fn render(f: &mut Frame, app: &mut App) {
    let items = app
        .episodes
        .iter()
        .map(|ep| format!("{:02} {}", ep.number, ep.title));
    let list = List::new(items).highlight_style(Style::new().add_modifier(Modifier::REVERSED));

    let area = f.area();

    let vlayout = Layout::vertical([Constraint::Min(0), Constraint::Percentage(20)]);
    let [upper, player] = vlayout.areas(area);

    let layout = Layout::horizontal([Constraint::Percentage(50)]).flex(Flex::Center);
    let [list_area] = layout.areas(upper);

    f.render_stateful_widget(list, list_area, &mut app.list_state);

    let player_layout = Layout::vertical([1, 1, 1]).flex(Flex::Center);
    let [song_title, indicators, controls] = player_layout.areas(player);

    let t = app.title.as_deref().unwrap_or("");

    let pause = if app.paused { "P" } else { " " };
    let mute = if app.muted { "M" } else { " " };

    let pos = match app.position {
        Some(pos) => {
            let (hours, mins, secs) = format_time(pos);
            format!("{:02}:{:02}:{:02}", hours, mins, secs)
        }
        None => "-".to_string(),
    };

    let dur = match app.duration {
        Some(dur) => {
            let (hours, mins, secs) = format_time(dur);
            format!("{:02}:{:02}:{:02}", hours, mins, secs)
        }
        None => "-".to_string(),
    };

    let title = Line::from(t).alignment(Alignment::Center);
    let ind = Line::from(format!("[{}] [{}]", pause, mute)).alignment(Alignment::Center);
    let info = Line::from(format!("{} / {}", pos, dur)).alignment(Alignment::Center);

    f.render_widget(title, song_title);
    f.render_widget(ind, indicators);
    f.render_widget(info, controls);
}
